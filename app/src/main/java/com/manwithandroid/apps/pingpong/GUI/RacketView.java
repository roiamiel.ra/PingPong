package com.manwithandroid.apps.pingpong.GUI;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Build;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.manwithandroid.apps.pingpong.R;

public class RacketView extends View {

    private static final int IN_EDIT_MODE_PERCENTAGE = 10;
    private static final int RACKET_WIDTH_PERCENTAGE = 30;
    private static final int RACKET_RADIUS           = 50;
    private static final int RACKET_COLOR            = 0xFFFFFFFF;

    private int mLayoutWidthPercentage  = -1;
    private int mLayoutHeightPercentage = -1;

    private boolean mRacketAlreadyInstall = false;
    private int mRacketWidth  = -1;
    private int mRacketHeight = -1;

    private int mRacketXPosition = -1;

    private Paint mViewPaint = null;
    private Canvas mViewCanvas = null;

    /**
     * Constructor without attrs param
     * @param context The view context
     */
    public RacketView(Context context) {
        super(context);

        //Set the touch listener
        installTouchListener();
    }

    /**
     * Constructor with attrs param
     * @param context The view context
     * @param attrs The attr params of the view
     */
    public RacketView(Context context, AttributeSet attrs) {
        super(context, attrs);

        //Attrs array reader
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.RacketView,
                0, 0
        );

        //Install attrs
        try {
            //Install percentage attrs
            //Width percentage attr
            mLayoutWidthPercentage = typedArray.getInt(
                    R.styleable.RacketView_layout_width_percentage,
                    mLayoutWidthPercentage
            );

            //Height percentage attrs
            mLayoutHeightPercentage = typedArray.getInt(
                    R.styleable.RacketView_layout_height_percentage,
                    mLayoutHeightPercentage
            );

        } finally {
            typedArray.recycle();
        }

        //Set the touch listener
        installTouchListener();
    }

    /**
     * This function set the touch listener
     */
    private void installTouchListener(){
        //this.setOnTouchListener(this);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        //Check for edit mode
        if(!isInEditMode()) {
            //Check if view in percentage

            super.setMeasuredDimension(
                    this.mLayoutWidthPercentage  != -1 ? (this.mLayoutWidthPercentage  * (getParentWidth(widthMeasureSpec)   / 100)) : MeasureSpec.getSize(widthMeasureSpec),
                    this.mLayoutHeightPercentage != -1 ? (this.mLayoutHeightPercentage * (getParentHeight(heightMeasureSpec) / 100)) : MeasureSpec.getSize(heightMeasureSpec)
            );

        } else {
            //If in edit mode make a evaluation calculation
            super.setMeasuredDimension(
                    this.mLayoutWidthPercentage  != -1 ? (this.mLayoutWidthPercentage  * IN_EDIT_MODE_PERCENTAGE) : MeasureSpec.getSize(widthMeasureSpec),
                    this.mLayoutHeightPercentage != -1 ? (this.mLayoutHeightPercentage * IN_EDIT_MODE_PERCENTAGE) : MeasureSpec.getSize(heightMeasureSpec)
            );

        }

        //Set the position of the rocket in the center of the area
        this.mRacketXPosition = getMeasuredWidth() / 2;
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);

        //Install the canvas
        if(this.mViewCanvas == null || canvas != null) {
            this.mViewCanvas = canvas;
        }

        //Install the view paint
        if(this.mViewPaint == null) {
            this.mViewPaint = new Paint();
            this.mViewPaint.setColor(RACKET_COLOR);
            this.mViewPaint.setStrokeWidth(10.0f);
        }

        //Install the racket params
        if(!this.mRacketAlreadyInstall) {
            installRacket();
        }

        //Draw the racket
        drawRacket();
    }

    /**
     * Draw the racket on the current position
     */
    private void drawRacket(){
        if(this.mViewCanvas != null){
            //Calculate the new x position of the left of the racket
            int newWidth = this.mRacketXPosition - this.mRacketWidth / 2;

            //Check the build version
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                //Draw a round rect
                this.mViewCanvas.drawRoundRect(
                        newWidth,
                        0,
                        newWidth + mRacketWidth,
                        mRacketHeight,
                        RACKET_RADIUS,
                        RACKET_RADIUS,
                        this.mViewPaint
                );

            } else {
                //Draw a basic rect
                this.mViewCanvas.drawRect(
                        newWidth,
                        0,
                        newWidth + mRacketWidth,
                        mRacketHeight,
                        this.mViewPaint
                );
            }
        }

    }

    /**
     * Install the params of the racket
     */
    private void installRacket(){
        this.mRacketWidth = getMeasuredWidth() / 100 * RACKET_WIDTH_PERCENTAGE;
        this.mRacketHeight = getMeasuredHeight();

        //Update the flag
        this.mRacketAlreadyInstall = true;

    }

    /**
     * Return the width of the parent
     */
    private int getParentWidth(int WidthMeasureSpec){
        return MeasureSpec.getSize(WidthMeasureSpec);
    }

    /**
     * Return the height of the parent
     */
    private int getParentHeight(int HeightMeasureSpec){
        return MeasureSpec.getSize(HeightMeasureSpec);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int x = (int) event.getX(),
                newPos = x + mRacketWidth / 2;

        if (x < mRacketWidth / 2) {
            x = mRacketWidth / 2;

        } else if (newPos > getMeasuredWidth()) {
            x = getMeasuredWidth() - mRacketWidth / 2;

        }

        this.mRacketXPosition = x;

        this.invalidate();

        return false;
    }
}
