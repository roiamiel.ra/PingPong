package com.manwithandroid.apps.pingpong;

import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;

import com.manwithandroid.apps.pingpong.GUI.RacketView;

public class MainActivity extends Activity {

    private RacketView mRacketView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.mRacketView = (RacketView) findViewById(R.id.racket_view);

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        this.mRacketView.onTouchEvent(event);

        return false;
    }
}
