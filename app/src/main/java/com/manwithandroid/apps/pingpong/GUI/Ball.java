package com.manwithandroid.apps.pingpong.GUI;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by RoiAm on 28/01/2017.
 */

public class Ball extends View {

    public Ball(Context context) {
        super(context);
    }

    public Ball(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

}
