package com.manwithandroid.apps.pingpong.GUI;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.Random;

public class SpaceLayout extends RelativeLayout {

    private static final int mRaindropInLine = 3;

    private static final int mBackgroundColor = 0x00000000;

    private static final int[][] mRaindropProximity = {
    //      Width    Height    Color Index  Speed
            {12,       90,        0,          25    },
            {15,       100,        0,         29    },
            {18,       120,        1,         33    },
            {22,       120,        2,         33    },
            {25,       130,        3,         37    },
            {28,       140,        4,         41    }
    };

    private static final int[] mColors = {
            0xff5d5858,
            0xff6e6a6a,
            0xff817d7d,
            0xff939090,
            0xffb7b5b5
    };

    private int mCreateNewRaindropLineAt = 18;
    private int mCreateNewRaindropLineCounter = 0;

    private boolean mRun = false;

    private View mThisView = null;
    private Looper mViewThreadLooper = null;

    private Paint mPaint = null;
    private Random mRandomGenerator;
    private ArrayList<Raindrop> mRaindropsList;
    private ArrayList<Integer> mIndexsToDeleteList;

    /**
     * Constructor without attrs param
     * @param context The view context
     */
    public SpaceLayout(Context context) {
        super(context);

        installBackground();
    }

    /**
     * Constructor with attrs param
     * @param context The view context
     * @param attrs The attr params of the layout
     */
    public SpaceLayout(Context context, AttributeSet attrs) {
        super(context, attrs);

        installBackground();
    }

    /**
     * Install the shooting stars background
     */
    private void installBackground(){
        mRaindropsList = new ArrayList<>();
        mIndexsToDeleteList = new ArrayList<>();
        mRandomGenerator = new Random();

        this.mPaint = new Paint();

        this.setBackgroundColor(this.mBackgroundColor);

        this.mRun = true;

        this.mThisView = this;
        this.mViewThreadLooper = Looper.myLooper();

        new Thread(() -> {
            while(this.mRun){
                updateView();
            }
        }).start();
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawColor(Color.BLACK);

        this.mIndexsToDeleteList.clear();
        if(this.mRun) {
            if (mCreateNewRaindropLineCounter == mCreateNewRaindropLineAt) {
                addNextRaindropLine();
                mCreateNewRaindropLineCounter = 0;
                System.gc();

            } else {
                mCreateNewRaindropLineCounter++;
            }

            Raindrop raindrop;
            for (int i = 0; i < mRaindropsList.size(); i++) {
                raindrop = mRaindropsList.get(i);

                this.mPaint.setColor(mColors[mRaindropProximity[raindrop.mProximity][2]]);

                raindrop.mY += mRaindropProximity[raindrop.mProximity][3];
                int h = this.getHeight() + 1000;

                if (raindrop.mY > h) {
                    this.mIndexsToDeleteList.add(i);

                } else {
                    canvas.drawRect(
                            raindrop.mX,
                            raindrop.mY,
                            raindrop.mX + mRaindropProximity[raindrop.mProximity][0],
                            raindrop.mY + mRaindropProximity[raindrop.mProximity][1],
                            this.mPaint
                    );
                }
            }
        }

        for (Integer integer : this.mIndexsToDeleteList) {
            this.mRaindropsList.remove(integer.intValue());
        }

        super.draw(canvas);
    }

    private void updateView(){
        new Handler(this.mViewThreadLooper).post(() -> {
            this.mThisView.invalidate();
        });

        try {
            Thread.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    private void addNextRaindropLine(){
        for (int i = 0; i < mRaindropProximity.length; i++) {
            for (int j = 0; j < mRaindropInLine; j++) {
                Raindrop raindrop = new Raindrop();
                raindrop.setX(mRandomGenerator.nextInt(this.getWidth() - mRaindropProximity[i][0]));
                raindrop.setY(-mRaindropProximity[i][1] - mRaindropProximity[i][3]);
                raindrop.setProximity(i);

                mRaindropsList.add(raindrop);
            }
        }
    }

    private class Raindrop {
        public float mX;
        public float mY;
        public int mProximity;

        public void setX(float X) {
            this.mX = X;
        }

        public void setY(float Y) {
            this.mY = Y;
        }

        public void setProximity(int Proximity) {
            this.mProximity = Proximity;
        }
    }
}
